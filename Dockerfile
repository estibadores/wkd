FROM registry.sindominio.net/debian as builder

ARG BRANCH=master

RUN apt-get update && \
    apt-get install -y --no-install-recommends git golang ca-certificates gnupg

RUN git clone https://git.autistici.org/ai3/tools/wkd/

WORKDIR /wkd
COPY signing.key /key
COPY signing.key /key
RUN gpg --import /key
RUN gpg --import /key
RUN LASTTAG=`git describe --tags --abbrev=0` && \
    git tag -v $LASTTAG && \
    git checkout $LASTTAG

run CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' ./cmd/wkdserver

FROM scratch

ENV TZ=Europe/Madrid

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /wkd/wkdserver /wkdserver
COPY wkd.yml /etc/wkd.yml

ENTRYPOINT ["/wkdserver"]
