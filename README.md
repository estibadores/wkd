WKD server
==========

Provides a [Web Key Directory](https://datatracker.ietf.org/doc/html/draft-koch-openpgp-webkey-service)
sever implementation, using a LDAP backend to retrieve the user OpenPGP keys.

Expectes the LDAP entries to contain the following fields:
* openPGPKeyHash. Contains the Z-Base-32 encoded SHA-1 of the username.
* openPGPKey. The binary form of the OpenPGP public key.
* mail. The full user email address.
